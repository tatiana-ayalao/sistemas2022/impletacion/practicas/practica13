﻿set NAMES 'utf8';
USE empleados;

/** consulta 1 Mostrar todos los campos y todos los registros de la tabla empleado **/

SELECT e.emp_no, e.apellido, e.oficio, e.dir, e.fecha_alt, e.salario, e.comision, e.dept_no 
FROM emple e;


/** consulta 2 Mostrar todos los campos y todos los registros de la tabla departamento **/

SELECT d.dept_no, d.dnombre, d.loc 
FROM depart d;


/** Consulta 3 Mostrar el apellido y oficio de cada empleado. **/

  SELECT e.oficio, e.apellido
  FROM emple e;

/** Consulta 4 Mostrar localización y número de cada departamento.**/

  SELECT d.loc, d.dept_no 
  FROM depart d;


/** Consulta 5 Mostrar el número, nombre y el primer carácter de la localización de cada
departamento **/

SELECT d.dept_no, d.dnombre, LEFT(d.loc,1) 
FROM depart d;

/**Consulta 6 Indicar el número de empleados que hay**/

  SELECT COUNT(*) 
  FROM emple e;


/** consulta 7 Datos de los empleados ordenados por apellido de forma ascendente **/

  SELECT e.emp_no, e.apellido, e.oficio, e.dir, e.fecha_alt, e.salario, e.comision, e.dept_no  
  FROM emple e
  ORDER BY e.apellido;


/** CONSULTA 8 Datos de los empleados ordenados por oficio de forma ascendente y después por
apellido de forma descendente **/

    SELECT e.emp_no, e.apellido, e.oficio, e.dir, e.fecha_alt, e.salario, e.comision, e.dept_no 
     FROM emple e
    ORDER BY e.oficio ASC, e.apellido DESC;


/** Consulta 9 Indicar el número de departamentos que hay **/

  SELECT COUNT(*) FROM depart d;


/** Consulta 10 Indicar el número de empleados más el número de departamentos     **/

SELECT (SELECT COUNT(*) FROM emple e)+(SELECT COUNT(*) FROM depart d) suma;


  /** Consulta 11  Datos de los dos primeros empleados ordenados por número de departamento
descendentemente.    **/

SELECT e.emp_no, e.apellido, e.oficio, e.dir, e.fecha_alt, e.salario, e.comision, e.dept_no FROM
 emple e 
ORDER BY e.dept_no DESC LIMIT 2;

  /** Consulta 12 Datos de los empleados ordenados por número de departamento descendentemente
y por oficio ascendente    **/

SELECT e.emp_no, e.apellido, e.oficio, e.dir, e.fecha_alt, e.salario, e.comision, e.dept_no FROM emple e 
ORDER BY e.dept_no DESC, e.oficio ;

  /** Consulta 13  Datos de los empleados ordenados por número de departamento descendentemente
y por el primer carácter del apellido ascendentemente.    **/

    SELECT e.emp_no, e.apellido, e.oficio, e.dir, e.fecha_alt, e.salario, e.comision, e.dept_no  
    FROM emple e
    ORDER BY e.dept_no DESC, LEFT(e.apellido,1) ASC;


  /** Consulta 14 Mostrar los códigos de los empleados cuyo salario sea mayor que 2000.  **/

SELECT e.emp_no 
FROM emple e
WHERE e.salario>2000;



  /** Consulta 15 Mostrar los códigos y apellidos de los empleados cuyo salario sea menor que 2000
y no tengan comision.     **/

SELECT e.emp_no, e.apellido 
FROM emple e
WHERE e.salario<2000;

  /** Consulta 16  Mostrar los datos de los empleados cuyo salario este entre 1500 y 2500    **/

SELECT e.emp_no, e.apellido, e.oficio, e.dir, e.fecha_alt, e.salario, e.comision, e.dept_no 
FROM emple e
WHERE e.salario BETWEEN 1500 AND 2500 ;


  /** Consulta 17 Mostrar los datos de los empleados cuyo oficio sea ʻANALISTAʼ     **/

SELECT e.emp_no, e.apellido, e.oficio, e.dir, e.fecha_alt, e.salario, e.comision, e.dept_no 
FROM emple e
WHERE e.oficio= 'Analista';

  /** Consulta 18 Mostrar el código de los empleados los empleados cuyo oficio sea ANALISTA y ganen
más de 2000 €. Realizar con AND y con INTERSECCION     **/

    SELECT e.emp_no, e.oficio 
    FROM emple e
    WHERE e.oficio= 'ANALISTA' AND e.salario>2000;

    SELECT * FROM (SELECT e.emp_no FROM emple e
    WHERE e.salario>2000) C1 NATURAL JOIN (SELECT e.emp_no FROM emple e WHERE e.oficio= "ANALISTA") C2 ;



  /** Consulta 19  Seleccionar el apellido y oficio de los empleados del departamento número 20.    **/

SELECT e.apellido, e.oficio FROM emple e
WHERE e.dept_no=20;


  /** Consulta 20  Contar el número de empleados cuyo oficio sea VENDEDOR    **/

    SELECT COUNT(*) FROM emple e
    WHERE e.oficio<>'vendedor';




  /** Consulta 21 Mostrar todos los datos de los empleados cuyos apellidos comiencen por m o por n
ordenados por apellido de forma ascendente.      **/
 
    SELECT e.emp_no, e.apellido, e.oficio, e.dir, e.fecha_alt, e.salario, e.comision, e.dept_no 
    FROM emple e
    WHERE e.apellido LIKE 'm%' OR 'n%'
    ORDER BY e.apellido ASC;

    SELECT 
      e.emp_no, e.apellido, e.oficio, e.dir, e.fecha_alt, e.salario, e.comision, e.dept_no 
      FROM emple e
    WHERE LEFT(e.apellido,1) IN ("m","n") ORDER BY e.apellido;



  /** Consulta 22 Seleccionar los empleados cuyo oficio sea ʻVENDEDORʼ. Mostrar los datos
ordenados por apellido de forma ascendente.     **/

    SELECT e.emp_no, e.apellido, e.oficio, e.dir, e.fecha_alt, e.salario, e.comision, e.dept_no 
    FROM emple e
    WHERE e.oficio='vendedor'
    ORDER BY e.apellido;





  /** Consulta 23   Mostrar los apellidos del empleado que mas gana    **/
SELECT e.apellido 
  FROM emple e
  ORDER BY e.salario
  LIMIT 1;

SELECT e.apellido 
FROM emple e
WHERE e.salario=(
SELECT MAX(salario)
FROM emple e1
);


  /** Consulta 24 Mostrar los empleados cuyo departamento sea 20 y cuyo oficio sea ʻANALISTAʼ.
Ordenar el resultado por apellido y oficio de forma ascendente. Realizarlo con AND
y con INTERSECCION      **/

    SELECT e.emp_no FROM emple e
      WHERE e.dept_no=20 AND e.oficio='Analista'
      ORDER BY e.apellido, e.oficio;

    SELECT * FROM 
      (SELECT e.emp_no* FROM emple e
      WHERE e.dept_no=20
      ORDER BY e.apellido, e.oficio) C1
NATURAL JOIN
      (SELECT e.emp_no FROM emple e
      WHERE e.oficio='Analista'
      ORDER BY e.apellido, e.oficio) C2 ;  


  /** Consulta 25 Realizar un listado de los distintos meses en que los empleados se
    han dado de alta      **/

    SELECT DISTINCT MONTH(e.fecha_alt) FROM emple e;

SELECT DISTINCT MONTHNAME(e.fecha_alt) FROM emple e;

  /** Consulta 26  Realizar un listado de los distintos años en que
    los empleados se han dado de alta      **/

SELECT DISTINCT YEAR(e.fecha_alt) FROM emple e;

 /** Consulta 27  Realizar un listado de los distintos días del mes en que los empleados se han dado
de alta     **/

SELECT DISTINCT DAYOFMONTH(e.fecha_alt) 
FROM emple e;

 /** Consulta 28 Mostrar los apellidos de los empleados que tengan un salario mayor que 2000 o que
pertenezcan al departamento número 20. Realizarlo con OR y con UNION       **/

  SELECT e.apellido FROM emple e
    WHERE e.salario>2000 OR e.dept_no=20;


SELECT DISTINCT e.apellido 
  FROM emple e WHERE e.salario>2000
  UNION
  SELECT DISTINCT e.apellido 
  FROM emple e
  WHERE e.dept_no=20;